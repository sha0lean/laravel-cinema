<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name', 50);
            $table->unsignedSmallInteger('capacity'); 
            /* UNSIGNED : 
             * integer adadptatif qui scale en fonction de l'input, mais ne change pas de category 
             * voir https://dev.mysql.com/doc/refman/8.0/en/integer-types.html 
             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
