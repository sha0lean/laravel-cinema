<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([[
            'name'      => 'big room',
            'capacity'  => '450',

        ],[
            'name'      => 'small room',
            'capacity'  => '250',

        ]]);
    }
}
