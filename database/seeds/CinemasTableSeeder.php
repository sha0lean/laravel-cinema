<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('cinemas')->insert([[
            'id'       => '1',
            'name'     => 'Cinemas La Praille',
            'street'   => 'Route des Jeunes 10, Lancy',
            'postcode' => '1212',
            'city'     => 'Geneve',
            'country'  => 'Suisse',
        ],[
            'id'       => '2',
            'name'     => 'Pathé Balexert',
            'street'   => 'Avenue Louis-Casaï 27',
            'postcode' => '1211',
            'city'     => 'Geneve',
            'country'  => 'Suisse',
        ],[
            'id'       => '3',
            'name'     => 'Ciné transat',
            'street'   => 'Rue de Lausanne',
            'postcode' => '1202',
            'city'     => 'Geneve',
            'country'  => 'Suisse',
        ]]);
    }
}
