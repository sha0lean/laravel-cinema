<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     
    public function run()
    {
        DB::table('artists')->insert([[
            'name'      => 'Francis Ford',
            'firstname' => 'Coppola',
            'birthday'  => '1939',
        ],[
            'name'      => 'Lynch',
            'firstname' => 'David',
            'birthday'  => '1946',
        ],[
            'name'      => 'Couscous',
            'firstname' => 'Abdel',
            'birthday'  => '1956',
        ],[
            'name'      => 'Dupond',
            'firstname' => 'David',
            'birthday'  => '1970',
        ],[
            'name'      => 'Montana',
            'firstname' => 'Tony',
            'birthday'  => '1948',
        ],[
            'name'      => 'LGD',
            'firstname' => 'Remy',
            'birthday'  => '1995',
        ],[
            'name'      => 'Bosson',
            'firstname' => 'Cyril',
            'birthday'  => '1987',
        ]]);
    }
}
