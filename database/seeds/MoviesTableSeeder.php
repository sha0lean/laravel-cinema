<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([[
            'title'     => 'Star wars 1',
            'year'      => '2000',
            'artist_id' => '1',
            'id'        => '1',
        ],[
            'title'     => 'Star wars 2',
            'year'      => '2001',
            'artist_id' => '2',
            'id'        => '2',
        ],[
            'title'     => 'Star wars 3',
            'year'      => '2002',
            'artist_id' => '3',
            'id'        => '3',
        ],[
            'title'     => 'Star wars 4',
            'year'      => '2003',
            'artist_id' => '4',
            'id'        => '4',
        ],[
            'title'     => 'Star wars 5',
            'year'      => '2004',
            'artist_id' => '5',
            'id'        => '5',
        ]]);
    }
}
