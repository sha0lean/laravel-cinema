<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model //NOS CHAMPS QUI SONT REMPLISSABLES : 
{
    protected $fillable = 
    [
        'name', 'firstname', 'birthday'
    ];

    public function has_played()
    {   
        return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
    }
}

// rien a voir avec la DB, c'est pour LARAVEL 