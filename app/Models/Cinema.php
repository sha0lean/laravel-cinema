<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = 
    [
        'name', 'street', 'postcode', 'city', 'country'
    ];
}
