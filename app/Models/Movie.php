<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    protected $fillable = [
        'title', 'year', 'artist_id', 'image'
    ];
    
    public function director() 
    {
        // un réalisateur 
        return $this->belongsTo(Artist::class,'artist_id');
        // return $this->belongsTo('App\Models\Artist');
    }

    public function actors()
    {
        // des acteurs peuvent jouer dans plusieurs films
        return $this->belongsToMany('App\Models\Artist');
    }

    use SoftDeletes;
}