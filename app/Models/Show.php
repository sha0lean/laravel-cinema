<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $fillable = 
    [
        'movie_id', 'startingtime'
    ];

    public function hours()
    {
        // des films peuvent avoir plusieurs horaires
        return $this->belongsToMany('App\Models\Movie');
    }

    use SoftDeletes;
}
