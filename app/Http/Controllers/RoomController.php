<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room; // pas besoin de back root 
use App\Http\Requests\MovieRequest; 

class RoomController extends Controller
{
    /** ---------------------------------------------------------------------------------------
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('rooms.index', ['rooms' => Room::paginate(0)]);
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    // va chercher dans "views/rooms/create.blade.php"
        return view('rooms.create'); 
    }

    /** ---------------------------------------------------------------------------------------
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    Room::create($request->all());
    return redirect()->route('room.create')
                     ->with('ok', __('Room has been saved'));
    }

    /** ---------------------------------------------------------------------------------------
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view ('rooms.edit', ['room' => $room]);
    }

    /** ---------------------------------------------------------------------------------------
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room->update ($request->all()); // update base de donnée

        return redirect()->route('room.index')
                         ->with('ok', __('Room has been updated'));
    }

    /** ---------------------------------------------------------------------------------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete(); // update base de donnée

        return response()->json();
    }
}
