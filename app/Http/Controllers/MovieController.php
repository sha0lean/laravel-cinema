<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Image;

// pas besoin de back root 
use App\Models\Movie; 
use App\Models\Artist; 
use App\Http\Requests\MovieRequest; 

class MovieController extends Controller
{
    /** ---------------------------------------------------------------------------------------
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // exemple
        // $artist = new Artist(['name' => 'Johnson', 'firsname' => 'Dwayne']);
        // $movie  = Movie::find(1);
        // $movie->actors()->save($artist);

        return view ('movies.index', ['movies' => Movie::paginate(0)]);
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    // va chercher dans "views/movies/create.blade.php"
        return view('movies.create', ['artists' => Artist::all()]);
    }

    /** ---------------------------------------------------------------------------------------
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $movie = Movie::create($request->all());
    
    //resize and save in a folder (not in the DB) 
    $poster = $request->file( 'image' );
    $filename = 'image_' . $movie->id . '.' . $poster->getClientOriginalExtension();
    
    Image::make( $poster )->fit( 180,240 )
    ->save( public_path( '/uploads/images/' . $filename ) );

    return redirect()->route('movie.create')
                     ->with('ok', __('Movie has been saved'));
    }

    /** ---------------------------------------------------------------------------------------
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //                   QUOI ? LEQUEL ? ( celui en paramètre )
    public function edit(Movie $movie)
    {
        return view ('movies.edit', ['movie' => $movie, 'artists' => Artist::all()]);
    }

    /** ---------------------------------------------------------------------------------------
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $movie->update ($request->all()); // update base de donnée

        return redirect()->route('movie.index')
                         ->with('ok', __('Movie has been updated'));
    }

    /** ---------------------------------------------------------------------------------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete(); // update base de donnée

        return response()->json();
    }
}
