<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cinema; // pas besoin de back root 
use App\Http\Requests\CinemaRequest; 


class CinemaController extends Controller
{
    /** ---------------------------------------------------------------------------------------
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('cinemas.index', ['cinemas' => Cinema::paginate(0)]);
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    // va chercher dans "views/cinemas/create.blade.php"
        return view('cinemas.create'); 
    }

    /** ---------------------------------------------------------------------------------------
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    Cinema::create($request->all());
    return redirect()->route('cinema.create')
                     ->with('ok', __('Cinema has been saved'));
    }

    /** ---------------------------------------------------------------------------------------
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cinema $cinema)
    {
        return view ('cinemas.edit', ['cinema' => $cinema]);
    }

    /** ---------------------------------------------------------------------------------------
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cinema $cinema)
    {
        $cinema->update ($request->all()); // update base de donnée

        return redirect()->route('cinema.index')
                         ->with('ok', __('Cinema has been updated'));
    }

    /** ---------------------------------------------------------------------------------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cinema $cinema)
    {
        $cinema->delete(); // update base de donnée

        return response()->json();
    }
}
