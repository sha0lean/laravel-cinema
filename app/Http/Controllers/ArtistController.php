<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// pas besoin de back root 
use App\Models\Artist; 
use App\Http\Requests\ArtistRequest; 

class ArtistController extends Controller
{
    /** ---------------------------------------------------------------------------------------
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Artist::paginate(0));
        // dd(Artist::all());
        return view ('artists.index', ['artists' => Artist::paginate(0)]);
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    // va chercher dans "views/artists/create.blade.php"
        return view('artists.create'); 
    }

    /** ---------------------------------------------------------------------------------------
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(\App\Http\Requests\ArtistRequest $request)
    {
        Artist::create($request->all());
        return redirect()->route('artist.create')
                         ->with('ok', __('Artist has been saved'));
    }

    /** ---------------------------------------------------------------------------------------
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /** ---------------------------------------------------------------------------------------
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //                   QUOI ? LEQUEL ? ( celui en paramètre)
    public function edit(Artist $artist)
    {
        return view ('artists.edit', ['artist' => $artist]);
    }

    /** ---------------------------------------------------------------------------------------
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtistRequest $request, Artist $artist)
    {
        $artist->update ($request->all()); // update base de donnée

        return redirect()->route('artist.index')
                         ->with('ok', __('Artist has been updated'));
    }

    /** ---------------------------------------------------------------------------------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        $artist->delete(); // update base de donnée

        return response()->json();
    }


    /** ---------------------------------------------------------------------------------------
     * Remove the specified resource from 
     * ===> s'assure d'utiliser ajax pour detruire
     */

    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }
}
