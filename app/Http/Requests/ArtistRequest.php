<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArtistRequest extends FormRequest

{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //TROUE
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     // va controller nos entrées
    public function rules()
    {
        $id = $this->artist ? ',' . $this->artist->id : '';

        return 
        [
            'name'      => 'required|string|max:20',
            'firstname' => 'required|string|max:15',
            'birthday'  => 'required|numeric|max:1000000',
        ];
    }
}
