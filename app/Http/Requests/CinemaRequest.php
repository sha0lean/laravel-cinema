<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CinemaRequest extends FormRequest

{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->cinema ? ',' . $this->cinema->id : '';

        return [
            'name'      => 'required|string|max:50',
            'street'    => 'required|string|max:50',
            'postcode'  => 'required|string|max:20',
            'city'      => 'required|string|max:20',
            'country'   => 'required|string|max:20',
        ];
    }
}
