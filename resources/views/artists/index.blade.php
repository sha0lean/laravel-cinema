@extends('layouts.app')

@section('content')

<button type="button" class="btn btn-lg btn-block btn-light mb-4 mt-4">
    <a href="/artist/create" title="@lang('create an artist')">
        CREATE
    </a>
</button>

<table class="table table-striped table-centered">
    <thead>
        {{-- ligne --}}
        <tr>
            {{-- entêtes --}}
            <th>{{__('Name')}} </th>
            <th>{{__('Firstname')}} </th>
            <th>{{__('Birthday')}} </th>
            <th>{{__('Actions')}} </th>
        </tr>
    </thead>

    <tbody>
        @foreach($artists as $artist)
        {{-- ligne --}}
        <tr>
            {{-- colonne --}}
            <td>{{$artist->name}} </td>
            {{-- colonne --}}
            <td>{{$artist->firstname}} </td>
            {{-- colonne --}}
            <td>{{$artist->birthday}} </td>

            {{-- colonne --}}
            <td class="table-action">
                <a type="button" href="{{ route('artist.edit', $artist->id )}}" class="btn btn-sm" data-toggle="tooltip"
                    title="@lang('Edit artist') {{ $artist->name}}">

                    <i class="fas fa-edit fa-lg"></i>
                </a>
                <a type="button" href="{{ route('artist.destroy', $artist->id )}}" class="btn btn-delete btn-danger btn-sm"
                    data-toggle="tooltip" title="@lang('Delete artist') {{ $artist->name}}">

                    <i class="fas fa-trash fa-lg"></i>
                </a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

{{ $artists->appends(request() -> except('page')) ->links() }}

<button type="button" class="btn btn-light btn-block">
    <a href="/movie"  title="@lang('GO TO MOVIES TABLE')">
        M O V I E S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/cinema"  title="@lang('GO TO CINEMA TABLE')">
        C I N E M A S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/room"  title="@lang('GO TO ROOM TABLE')">
    R O O M S
    </a>
</button>

<script>

    $.ajaxSetup({
        // CSRF id les forms pour valider, la on a de l'ajax,
        // on rajoute dans toute les entetes le csrf ajax.
        headers: {
        // va chercher dans   la b meta,                  son contenu
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    $(document).on('click', '.btn-delete', function () {

        let button = $(this);

        $.ajax({ // quand clic on send request aux serv
            url: button.attr('href'),
            type: 'DELETE'
        }).done(function(){
            button.closest('tr').remove();
        });
        return false;
    });

</script>

@endsection
