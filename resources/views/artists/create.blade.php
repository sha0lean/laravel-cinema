@extends('layouts.app')

@section('content')
{{-- du yield * --}}

<form method="POST" action="{{route('artist.store')}}">

    {{ csrf_field() }} {{-- protection contre les injections SQL (champs caché blabla) --}}
<br>
    <p>
        <label for="name"> Name </label>
        <input type="text" name="name" id="name" value="" required />
<br><br>
        <label for="name"> First name </label>
        <input type="text" name="firstname" id="firstname" value="" required />
<br><br>
        <label for="name"> Birthyear </label>
        <input type="number" name="birthdate" id="birthdate" value="" required />
    </p>

    <button type="submit" class="btn-primary btn-block"> Create </button>

</form>

<button type="button" class="btn btn-lg btn-block btn-light mb-4 mt-4">
    <a href="/artist" title="@lang('BACK TO ARTISTS')">
        BACK TO ARTISTS
    </a>
</button>

@endsection
