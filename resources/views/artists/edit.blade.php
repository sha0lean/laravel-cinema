@extends('layouts.app')

@section('content')
{{-- du yield * --}}


<form method="POST" action="{{route('artist.update', $artist->id)}}">


    {{ csrf_field() }} {{-- protection contre les injections SQL (champs caché blabla) --}}
    {{method_field('PUT')}}

    <p>
        <label for="name"> Name </label>
        <input type="text" name="name" id="name" value="{{$artist->name}}" required />

        <label for="firstname"> First name </label>
        <input type="text" name="firstname" id="firstname" value="{{$artist->firstname}}" required />
    </p>

    <p>
        <label for="birthday"> Birthyear </label>
        <input type="number" name="birthday" id="birthday" value="{{$artist->Birthdate}}" required />
    </p>

    <button type="submit"> Create </button>

</form>

@endsection
