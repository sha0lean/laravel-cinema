@extends('layouts.app')

@section('content')
    {{-- du yield * --}}
        
    <form method="POST" action="{{route('room.store')}}">
        
        {{ csrf_field() }}  {{-- protection contre les injections SQL (champs caché blabla) --}}
        
        <p>
            <label for= "name"> Name </label>
            <input type="text" name="name" id="name" value="" required />

            <label for= "capacity"> Capacity </label>
            <input type="number" name="capacity" id="capacity" value="" required />
        </p>
        
        <button type="submit"> Create </button>
        
    </form>

@endsection