@extends('layouts.app')

@section('content')

<button type="button" class="btn btn-lg btn-block btn-light mb-4 mt-4">
    <a href="/room/create" title="@lang('create a room')">
        CREATE
    </a>
</button>

<table class="table table-striped table-centered">
    <thead>
        {{-- ligne --}}
        <tr>
            {{-- entêtes --}}
            <th>{{__('Name')}} </th>
            <th>{{__('Capacity')}} </th>
        </tr>
    </thead>

    <tbody>
        @foreach($rooms as $room)
        {{-- ligne --}}
        <tr>
            {{-- colonne --}}
            <td>{{$room->name}} </td>
            {{-- colonne --}}
            <td>{{$room->capacity}} </td>
            {{-- colonne --}}
            <td class="table-action">
                <a type="button" href="{{ route('room.edit', $movie->id )}}" class="btn btn-sm" data-toggle="tooltip"
                  title="@lang('Edit room') {{ $room->title}}">

                    <i class="fas fa-edit fa-lg"></i>
                </a>
                <a type="button" href="{{ route('room.destroy', $room->id )}}" class="btn btn-delete btn-danger btn-sm"
                    data-toggle="tooltip" title="@lang('Delete room') {{ $movie->title}}">

                    <i class="fas fa-trash fa-lg"></i>
                </a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
{{ $rooms->appends(request() -> except('page')) ->links() }}

<button type="button" class="btn btn-light btn-block">
    <a href="/movie"  title="@lang('GO TO MOVIES TABLE')">
        M O V I E S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/cinema"  title="@lang('GO TO CINEMA TABLE')">
        C I N E M A S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/artist"  title="@lang('GO TO ARTIST TABLE')">
    A R T I S T S
    </a>
</button>

<script>

    $.ajaxSetup({
        // CSRF id les forms pour valider, la on a de l'ajax,
        // on rajoute dans toute les entetes le csrf ajax.
        headers: {
        // va chercher dans   la b meta,                  son contenu
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    $(document).on('click', '.btn-delete', function () {

        let button = $(this);

        $.ajax({ // quand clic on send request aux serv
            url: button.attr('href'),
            type: 'DELETE'
        }).done(function(){
            button.closest('tr').remove();
        });
        return false;
    });

</script>

@endsection
