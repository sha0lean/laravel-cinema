@extends('layouts.app')

@section('content')
{{-- du yield * --}}


<form method="POST" action="{{route('room.update', $room->id)}}">


    {{ csrf_field() }} {{-- protection contre les injections SQL (champs caché blabla) --}}
    {{method_field('PUT')}}

    <p>
        <label for="name"> Name </label>
        <input type="text" name="name" id="name" value="{{$room->name}}" required />

        <label for="capacity"> Capacity </label>
        <input type="number" name="capacity" id="capacity" value="{{$room->capacity}}" required />
        
    </p>
    <button type="submit"> Create </button>

</form>

@endsection
