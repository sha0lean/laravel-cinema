@extends('layouts.app')

@section('content')
    {{-- du yield * --}}
        
    <form method="POST" action="{{route('cinema.store')}}" enctype="multipart/form-data">
        
        {{ csrf_field() }}  {{-- protection contre les injections SQL (champs caché blabla) --}}
        
        <p>
            <br>
            <label for= "name"> Name </label>
            <input type="text" name="name" id="name" value="" required />
            <br><br>
            <label for= "street"> Street </label>
            <input type="text" name="street" id="street" value="" required />
            <br><br>
            <label for= "postcode"> Postcode </label>
            <input type="text" name="postcode" id="postcode" value="" required />
            <br><br>
            <label for= "city"> City </label>
            <input type="text" name="city" id="city" value="" required />
            <br><br>
            <label for= "country"> Country </label>
            <input type="text" name="country" id="country" value="" required />
        </p>
        
        <button type="submit" class="btn-primary btn-block"> Create </button>
    </form>

@endsection 