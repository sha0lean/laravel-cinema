@extends('layouts.app')

@section('content')
{{-- du yield * --}}


<form method="POST" action="{{route('cinema.update', $cinema->id)}}">


    {{ csrf_field() }} {{-- protection contre les injections SQL (champs caché blabla) --}}
    {{method_field('PUT')}}

    <p>
        <br>
        <label for= "name"> Name </label>
        <input type="text" name="name" id="name" value="{{$cinema->name}}" required />
        <br><br>
        <label for= "street"> Street </label>
        <input type="text" name="street" id="street" value="{{$cinema->street}}" required />
        <br><br>
        <label for= "postcode"> Postcode </label>
        <input type="text" name="postcode" id="postcode" value="{{$cinema->postcode}}" required />
        <br><br>
        <label for= "city"> City </label>
        <input type="text" name="city" id="city" value="{{$cinema->city}}" required />
        <br><br>
        <label for= "country"> Country </label>
        <input type="text" name="country" id="country" value="{{$cinema->country}}" required />
    </p>
    <button type="submit"> Create </button>

</form>

@endsection
