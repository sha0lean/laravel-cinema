@extends('layouts.app')

@section('content')

<button type="button" class="btn btn-lg btn-block btn-light mb-4 mt-4">
    <a href="/cinema/create" title="@lang('create a cinema')">
        CREATE
    </a>
</button>

<table class="table table-striped table-centered">
    <thead>
        {{-- ligne --}}
        <tr>
            {{-- entêtes --}}
            <th>{{__('Name')}} </th>
            <th>{{__('Street')}} </th>
            <th>{{__('Postcode')}} </th>
            <th>{{__('City')}} </th>
            <th>{{__('Country')}} </th>
        </tr>
    </thead>

    <tbody>
        @foreach($cinemas as $cinema)
        {{-- ligne --}}
        <tr>
            <td>{{$cinema->name}} </td>
            <td>{{$cinema->street}} </td>
            <td>{{$cinema->postcode}} </td>
            <td>{{$cinema->city}} </td>
            <td>{{$cinema->country}} </td>

            <td class="table-action">
                <a type="button" href="{{ route('cinema.edit', $cinema->id )}}" class="btn btn-sm" data-toggle="tooltip"
                    title="@lang('Edit cinema') {{ $cinema->name}}">

                    <i class="fas fa-edit fa-lg"></i>
                </a>
                <a type="button" href="{{ route('cinema.destroy', $cinema->id )}}"
                    class="btn btn-delete btn-danger btn-sm" data-toggle="tooltip"
                    title="@lang('Delete cinema') {{ $cinema->name}}">

                    <i class="fas fa-trash fa-lg"></i>
                </a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
{{ $cinemas->appends(request() -> except('page')) ->links() }}


<button type="button" class="btn btn-light btn-block">
    <a href="/artist"  title="@lang('GO TO ARTIST TABLE')">
        A R T I S T E S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/cinema"  title="@lang('GO TO CINEMA TABLE')">
        C I N E M A S
    </a>
</button>

<button type="button" class="btn btn-light btn-block">
    <a href="/room"  title="@lang('GO TO ROOM TABLE')">
    R O O M S
    </a>
</button>

<script>
    $.ajaxSetup({
        // CSRF id les forms pour valider, la on a de l'ajax,
        // on rajoute dans toute les entetes le csrf ajax.
        headers: {
            // va chercher dans   la b meta,                  son contenu
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    $(document).on('click', '.btn-delete', function () {

        let button = $(this);

        $.ajax({ // quand clic on send request aux serv
            url: button.attr('href'),
            type: 'DELETE'
        }).done(function () {
            button.closest('tr').remove();
        });
        return false;
    });

</script>

@endsection
