@extends('layouts.app')

@section('content')
    {{-- du yield * --}}
        
    <form method="POST" action="{{route('movie.store')}}" enctype="multipart/form-data">
        
        {{ csrf_field() }}  {{-- protection contre les injections SQL (champs caché blabla) --}}
        
        <p>
            <br>
            <label for= "title"> Title </label>
            <input type="text" name="title" id="title" value="" required />
            <br><br>
            <label for= "year"> Year </label>
            <input type="text" name="year" id="year" value="" required />
            <br><br>
            <label for="artist_id">Realisator:</label>
            <select name="artist_id" required>
                @foreach($artists as $artist)
                <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                @endforeach
            </select>
            <br><br>
                Select image to upload:
                <input type="file" name="fileToUpload" id="image" accept=".png,.jpeg,.jpg">
        </p>
        
        <button type="submit" class="btn-primary btn-block"> Create </button>
    </form>

    <button type="button" class="btn btn-lg btn-block btn-light mb-4 mt-4">
        <a href="/movie" title="@lang('BACK TO MOVIES')">
            BACK TO MOVIES
        </a>
    </button>

@endsection 