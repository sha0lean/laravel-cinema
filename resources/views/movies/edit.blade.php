@extends('layouts.app')

@section('content')
{{-- du yield * --}}


<form method="POST" action="{{route('movie.update', $movie->id)}}">


    {{ csrf_field() }} {{-- protection contre les injections SQL (champs caché blabla) --}}
    {{method_field('PUT')}}

    <p>
        <label for="title"> Name </label>
        <input type="text" name="title" id="title" value="{{$movie->title}}" required />

        <label for="year"> Year </label>
        <input type="number" name="year" id="year" value="{{$movie->year}}" required />
        
        <label for="artist_id">Realisator:</label>
        <select name="artist_id" required>
            @foreach($artists as $artist)
            <option value="{{ $artist->id }}">{{ $artist->name }}</option>
            @endforeach
        </select>
    </p>
    <button type="submit"> Create </button>

</form>

@endsection
